if [ $# -lt 1 ]; then
    >&2 echo "Usage: bash $0 path [exceptions]"
    exit 1
fi

path=$1
shift
exceptions=$@

function is_in() {
    srch=$1
    shift
    pile=$@
    for word in $pile; do
        if [[ $word == $srch ]]; then
            return 0
        fi
    done
    return 1
}

dirname=`basename $path`
for file in $path/*.c; do
    filename=`basename $file`
    if is_in $filename $exceptions; then continue; fi
    if [[ ! $filename == "$dirname"_* ]]; then
        mv $file "$path/$dirname"_"$filename"
    fi
done
