# ADGraphicsMagick conda recipe

Home: "https://github.com/areaDetector/ADSupport/tree/master/supportApp/GraphicsMagickSrc"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: ADGraphicsMagick library
